package ru.t1.dkozoriz.tm.dto.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "login")
    private String login;

    @NotNull
    @Column(nullable = false, name = "password")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}