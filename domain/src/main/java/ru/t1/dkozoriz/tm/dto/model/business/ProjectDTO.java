package ru.t1.dkozoriz.tm.dto.model.business;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
public final class ProjectDTO extends BusinessModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    public ProjectDTO(@NotNull final String name) {
        super(name);
    }

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

}