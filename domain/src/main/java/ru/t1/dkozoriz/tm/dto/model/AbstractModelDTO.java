package ru.t1.dkozoriz.tm.dto.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractModelDTO implements Serializable {

    @NotNull
    @Id
    @Column(name = "row_id")
    private String id = UUID.randomUUID().toString();

}