package ru.t1.dkozoriz.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;

import java.util.List;

public interface ITaskRepository  {

    @Insert(
            "INSERT INTO tm_task (row_id, user_id, project_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description},  #{status}, #{created})"
    )
    void add(@NotNull TaskDTO task);

    @Insert(
            "INSERT INTO tm_task (row_id, user_id, project_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description},  #{status}, #{created})"
    )
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull TaskDTO task);

    @Update(
            "UPDATE tm_task " +
                    "SET name = #{name}, description = #{description}, status = #{status} WHERE row_id = #{id}"
    )
    void update(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    TaskDTO findByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    TaskDTO findByIdWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    TaskDTO findByIndexWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_task")
    int getSize();

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSizeWithUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE row_id = #{id}")
    void remove(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE row_id = #{p.id} AND user_id = #{userId}")
    void removeWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("p") TaskDTO task);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

}