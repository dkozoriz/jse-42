package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    void add(@NotNull SessionDTO session);

    void clear(@Nullable String userId);

    void clear();

    void remove(@Nullable SessionDTO session);

    List<SessionDTO> findAll(@Nullable String userId);

    SessionDTO findById(@Nullable String id);

}