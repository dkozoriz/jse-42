package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    int getSize(@Nullable String userId);

    List<ProjectDTO> findAll(@Nullable String userId);

    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    ProjectDTO findById(@Nullable String userId, @Nullable String id);

    ProjectDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    public ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    ProjectDTO changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    ProjectDTO changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}