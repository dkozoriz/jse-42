package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.LoginUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.LogoutUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.ViewProfileUserResponse;
import ru.t1.dkozoriz.tm.exception.EndpointException;
import ru.t1.dkozoriz.tm.dto.model.SessionDTO;
import ru.t1.dkozoriz.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @NotNull
    @Override
    @WebMethod
    public LoginUserResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final String token;
        try {
            token = getServiceLocator().getAuthService().login(login, password);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new LoginUserResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public LogoutUserResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        try {
            getServiceLocator().getAuthService().invalidate(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new LogoutUserResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ViewProfileUserResponse getProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final UserDTO user;
        try {
            user = getServiceLocator().getUserService().findById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ViewProfileUserResponse(user);
    }

}