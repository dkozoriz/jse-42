package ru.t1.dkozoriz.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert(
            "INSERT INTO tm_project (row_id, user_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{name}, #{description},  #{status}, #{created})"
    )
    void add(@NotNull ProjectDTO project);

    @Insert(
            "INSERT INTO tm_project (row_id, user_id, name, description, status, created) " +
                    "VALUES (#{id}, #{userId}, #{name}, #{description},  #{status}, #{created})"
    )
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull ProjectDTO project);

    @Update(
            "UPDATE tm_project " +
                    "SET name = #{name}, description = #{description}, status = #{status} WHERE row_id = #{id}"
    )
    void update(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findByIndex(@NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findByIdWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT #{index}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findByIndexWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm_project")
    int getSize();

    @Nullable
    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    Integer getSizeWithUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE row_id = #{p.id} AND user_id = #{userId}")
    void removeWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("p") ProjectDTO project);

}