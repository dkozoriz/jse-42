package ru.t1.dkozoriz.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Insert(
            "INSERT INTO tm_session (row_id, user_id, created, role) " +
                    "VALUES (#{id}, #{userId}, #{date}, #{role})"
    )
    void add(@NotNull SessionDTO session);

    @Insert(
            "INSERT INTO tm_session (row_id, user_id, created, role) " +
                    "VALUES (#{session.id}, #{userId}, #{session.date}, #{session.role})"
    )
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("session") SessionDTO session);

    @Update(
            "UPDATE tm_session SET locked = #{locked}, first_name = #{firstName}, middle_name = #{middleName} " +
                    ", last_name = #{lastName}, password = #{passwordHash}, role = #{role} WHERE row_id = #{id}"
    )
    void update(@NotNull SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clearWithUserId(@Nullable String userId);

    @Delete("DELETE FROM tm_session")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id")
    })
    SessionDTO findById(@Nullable String id);

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSizeWithUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT COUNT(*) FROM tm_session")
    int getSize();

    @Delete("DELETE FROM tm_session WHERE row_id = #{id}")
    void removeWithUserId(@Nullable String userId, @Nullable SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE row_id = #{id}")
    void remove(@Nullable SessionDTO session);

}