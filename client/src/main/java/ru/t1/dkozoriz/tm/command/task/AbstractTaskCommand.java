package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public AbstractTaskCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public Role[] getRoles() {
        return Role.values();
    }

    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECTID: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}