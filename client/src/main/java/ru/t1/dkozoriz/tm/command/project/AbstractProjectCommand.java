package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDTO;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public AbstractProjectCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}