package ru.t1.dkozoriz.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.SoapCategory;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.model.UserDTO;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final List<UserDTO> userList = new ArrayList<>();

    @BeforeClass
    public static void setConnection() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE.setToken(adminToken);
    }

    @Before
    public void initTest() {
        final int numberOfUsers = 10;
        for (int i = 1; i <= numberOfUsers; i++) {
            @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                    "login" + i,
                    "password" + i,
                    "email" + i
            );
            @Nullable final UserDTO user = USER_ENDPOINT.userRegistry(request).getUser();
            Assert.assertNotNull(user);
            userList.add(user);
        }
    }

    @Test
    public void testUserLock() {
        @NotNull final String login = userList.get(0).getLogin();
        @NotNull final UserLockRequest requestLock = new UserLockRequest(TOKEN_SERVICE.getToken(), login);
        USER_ENDPOINT.userLock(requestLock);
        @NotNull final UserFindByLoginRequest requestFind = new UserFindByLoginRequest(TOKEN_SERVICE.getToken(), login);
        @NotNull final UserDTO currentUser = USER_ENDPOINT.userFindByLogin(requestFind).getUser();
        Assert.assertEquals(true, currentUser.getLocked());
    }

    @Test
    public void testUserUnlock() {
        @NotNull final String login = userList.get(0).getLogin();
        @NotNull final UserLockRequest requestLock = new UserLockRequest(TOKEN_SERVICE.getToken(), login);
        USER_ENDPOINT.userLock(requestLock);
        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(TOKEN_SERVICE.getToken(), login);
        USER_ENDPOINT.userUnlock(requestUnlock);
        @NotNull final UserFindByLoginRequest requestFind = new UserFindByLoginRequest(TOKEN_SERVICE.getToken(), login);
        @NotNull final UserDTO currentUser = USER_ENDPOINT.userFindByLogin(requestFind).getUser();
        Assert.assertEquals(false, currentUser.getLocked());
    }

    @Test
    public void testUserRegistryAndRemove() {
        @NotNull final String login = "test login";
        @NotNull final String password = "test password";
        @NotNull final String email = "email@test.tu";
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final UserDTO user = USER_ENDPOINT.userRegistry(request).getUser();
        Assert.assertNotNull(user);
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(TOKEN_SERVICE.getToken(), login);
        USER_ENDPOINT.userRemove(requestRemove);
    }

    @Test
    public void testUserChangePassword() {
        @NotNull final String login = userList.get(1).getLogin();
        @NotNull final String password = "password2";
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, password);
        @Nullable final String userToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(userToken);
        @NotNull final String passwordNew = "password new";
        @NotNull final UserChangePasswordRequest requestPassword = new UserChangePasswordRequest(userToken, passwordNew);
        USER_ENDPOINT.userChangePassword(requestPassword);
        @NotNull final UserLoginRequest loginRequestNewPass = new UserLoginRequest(login, passwordNew);
        Assert.assertNotNull(AUTH_ENDPOINT.login(loginRequestNewPass).getToken());
    }

    @Test
    public void testUserUpdateProfile() {
        @NotNull final String firstName = "Имя";
        @NotNull final String lastName = "Фамилия";
        @NotNull final String middleName = "Отчество";
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(
                TOKEN_SERVICE.getToken(), firstName, middleName, lastName);
        @NotNull final UserDTO currentUser = USER_ENDPOINT.userUpdateProfile(request).getUser();
        Assert.assertNotNull(currentUser);
        Assert.assertEquals(firstName, currentUser.getFirstName());
        Assert.assertEquals(lastName, currentUser.getLastName());
        Assert.assertEquals(middleName, currentUser.getMiddleName());
    }

    @After
    public void after() {
        final int numberOfUsers = 10;
        for (int i = 1; i <= numberOfUsers; i++) {
            @NotNull final UserRemoveRequest request = new UserRemoveRequest(TOKEN_SERVICE.getToken(), "login" + i);
            Assert.assertNotNull(USER_ENDPOINT.userRemove(request));
        }
    }

}