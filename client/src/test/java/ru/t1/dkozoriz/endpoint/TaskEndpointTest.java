package ru.t1.dkozoriz.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.SoapCategory;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserRemoveRequest;
import ru.t1.dkozoriz.tm.dto.response.task.CreateTaskResponse;
import ru.t1.dkozoriz.tm.dto.response.task.ShowListTaskResponse;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDTO;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.service.TokenService;

import java.util.ArrayList;
import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final ITaskEndpoint TASK_ENDPOINT = ITaskEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final List<TaskDTO> taskList = new ArrayList<>();

    @Nullable
    private ProjectDTO project = new ProjectDTO();

    private final int numberOfTasks = 10;

    @BeforeClass
    public static void setConnection() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("test_user", "test", null);
        USER_ENDPOINT.userRegistry(registryRequest);
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("test_user", "test");
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE.setToken(adminToken);
    }

    @Before
    public void initTest() {

        @NotNull final ProjectCreateRequest requestProject = new ProjectCreateRequest(
                TOKEN_SERVICE.getToken(), "Project", "Description"
        );
        project = PROJECT_ENDPOINT.projectCreate(requestProject).getProject();
        for (int i = 1; i <= numberOfTasks; i++) {
            @NotNull final TaskCreateRequest request = new TaskCreateRequest(
                    TOKEN_SERVICE.getToken(),
                    "Task " + i,
                    "Description " + i
            );
            @Nullable final TaskDTO task = TASK_ENDPOINT.taskCreate(request).getTask();
            Assert.assertNotNull(task);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final String projectId = project.getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                TOKEN_SERVICE.getToken(), projectId, taskId
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskBindToProject(request).getTask();
        Assert.assertEquals(projectId, currentTask.getProjectId());
    }

    @Test
    public void testUnbindTaskToProject() {
        @NotNull final String projectId = project.getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                TOKEN_SERVICE.getToken(), projectId, taskId
        );
        TASK_ENDPOINT.taskBindToProject(requestBind);

        @NotNull final TaskUnbindToProjectRequest requestUnbind = new TaskUnbindToProjectRequest(
                TOKEN_SERVICE.getToken(), projectId, taskId
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskUnbindToProject(requestUnbind).getTask();
        Assert.assertNull(currentTask.getProjectId());
    }


    @Test
    public void testShowTasksByProjectId() {
        @NotNull final String projectId = project.getId();
        for (int i = 1; i <= numberOfTasks/2; i++) {
            @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                    TOKEN_SERVICE.getToken(), projectId, taskList.get(i).getId()
            );
            TASK_ENDPOINT.taskBindToProject(requestBind);
        }
        @NotNull final TaskShowAllByProjectIdRequest request = new TaskShowAllByProjectIdRequest(
                TOKEN_SERVICE.getToken(), projectId
        );
        @NotNull final List<TaskDTO> currentTasks = TASK_ENDPOINT.taskShowAllByProjectId(request).getTaskList();
        Assert.assertEquals(numberOfTasks/2, currentTasks.size());
    }

    @Test
    public void testShowList() {
        @NotNull final TaskShowListRequest request = new TaskShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowListTaskResponse response = TASK_ENDPOINT.listTask(request);
        @NotNull List<TaskDTO> list = response.getTaskList();
        Assert.assertEquals(taskList.size(), list.size());
    }

    @Test
    public void testShowTaskById() {
        for (@NotNull final TaskDTO task : taskList) {
            @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(TOKEN_SERVICE.getToken(), task.getId());
            @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskShowById(request).getTask();
            Assert.assertNotNull(currentTask);
            Assert.assertEquals(currentTask.getId(), task.getId());
        }
    }

    @Test
    public void testShowTaskByIndex() {
        for (int i = 1; i < taskList.size(); i++) {
            @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(TOKEN_SERVICE.getToken(), i);
            @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskShowByIndex(request).getTask();
            Assert.assertNotNull(currentTask);
            Assert.assertEquals(currentTask.getName(), taskList.get(i).getName());
        }
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(
                TOKEN_SERVICE.getToken(), taskId, status
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskChangeStatusById(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(status, currentTask.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(
                TOKEN_SERVICE.getToken(), taskIndex, status
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskChangeStatusByIndex(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(status, currentTask.getStatus());
    }

    @Test
    public void testCompleteTaskById() {
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(
                TOKEN_SERVICE.getToken(), taskId
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskCompleteById(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(Status.COMPLETED, currentTask.getStatus());
    }

    @Test
    public void testCompleteTaskByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final TaskCompleteByIndexRequest request
                = new TaskCompleteByIndexRequest(TOKEN_SERVICE.getToken(), taskIndex);
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskCompleteByIndex(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(Status.COMPLETED, currentTask.getStatus());
    }

    @Test
    public void testStartTaskById() {
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(
                TOKEN_SERVICE.getToken(), taskId
        );
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskStartById(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(Status.IN_PROGRESS, currentTask.getStatus());
    }

    @Test
    public void testStartTaskByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final TaskStartByIndexRequest request
                = new TaskStartByIndexRequest(TOKEN_SERVICE.getToken(), taskIndex);
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskStartByIndex(request).getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(Status.IN_PROGRESS, currentTask.getStatus());
    }

    @Test
    public void testTaskCreate() {
        @NotNull final String name = "Test name";
        @NotNull final String description = "Test description";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(TOKEN_SERVICE.getToken(), name, description);
        @NotNull final CreateTaskResponse response = TASK_ENDPOINT.taskCreate(request);
        @Nullable final TaskDTO currentTask = response.getTask();
        Assert.assertNotNull(currentTask);
        Assert.assertEquals(currentTask.getName(), name);
        Assert.assertEquals(currentTask.getDescription(), description);
    }

    @Test
    public void testClearTaskList() {
        @NotNull final TaskListClearRequest clearRequest = new TaskListClearRequest(TOKEN_SERVICE.getToken());
        TASK_ENDPOINT.taskListClear(clearRequest);
        @NotNull final TaskShowListRequest showRequest = new TaskShowListRequest(TOKEN_SERVICE.getToken(), null);
        @NotNull final ShowListTaskResponse showResponse = TASK_ENDPOINT.listTask(showRequest);
        @Nullable final List<TaskDTO> currentTaskList = showResponse.getTaskList();
        Assert.assertNull(currentTaskList);
    }

    @Test
    public void testRemoveTaskById() {
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final int oldSize = taskList.size();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(TOKEN_SERVICE.getToken(), taskId);
        TASK_ENDPOINT.taskRemoveById(request);
        @NotNull final TaskShowListRequest showRequest = new TaskShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowListTaskResponse response = TASK_ENDPOINT.listTask(showRequest);
        @NotNull List<TaskDTO> currentList = response.getTaskList();
        Assert.assertEquals(currentList.size(), oldSize-1);
    }

    @Test
    public void testRemoveTaskByIndex() {
        @NotNull final Integer taskIndex = 0;
        @NotNull final int oldSize = taskList.size();
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(TOKEN_SERVICE.getToken(), taskIndex);
        TASK_ENDPOINT.taskRemoveByIndex(request);
        @NotNull final TaskShowListRequest showRequest = new TaskShowListRequest(TOKEN_SERVICE.getToken(), null);
        @Nullable final ShowListTaskResponse response = TASK_ENDPOINT.listTask(showRequest);
        @NotNull List<TaskDTO> currentList = response.getTaskList();
        Assert.assertEquals(currentList.size(), oldSize-1);
    }

    @Test
    public void testUpdateTaskById() {
            @NotNull final String name = "new Task";
            @NotNull final String description = "new Description";
            @NotNull final String taskId = taskList.get(0).getId();
            @NotNull final TaskUpdateByIdRequest request
                    = new TaskUpdateByIdRequest(TOKEN_SERVICE.getToken(), taskId, name, description);
            @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskUpdateById(request).getTask();
            Assert.assertEquals(currentTask.getName(), name);
            Assert.assertEquals(currentTask.getDescription(), description);
    }

    @Test
    public void testUpdateTaskByIndex() {
        @NotNull final String name = "new Task";
        @NotNull final String description = "new Description";
        @NotNull final Integer taskIndex = 0;
        @NotNull final TaskUpdateByIndexRequest request
                = new TaskUpdateByIndexRequest(TOKEN_SERVICE.getToken(), taskIndex, name, description);
        @Nullable final TaskDTO currentTask = TASK_ENDPOINT.taskUpdateByIndex(request).getTask();
        Assert.assertEquals(currentTask.getName(), name);
        Assert.assertEquals(currentTask.getDescription(), description);
    }

    @After
    public void after() {
        @NotNull final TaskListClearRequest request = new TaskListClearRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(TASK_ENDPOINT.taskListClear(request));
    }

    @AfterClass
    public static void clearUser() {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest("admin", "admin");
        @Nullable final String token = AUTH_ENDPOINT.login(requestLogin).getToken();
        Assert.assertNotNull(token);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(token, "test_user");
        USER_ENDPOINT.userRemove(removeRequest);
    }

}